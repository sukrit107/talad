import React from 'react';
import { TextInput, Image, WebView, StyleSheet, Text, View, TouchableOpacity, Linking } from 'react-native';
import axios from 'axios';
import { Icon } from 'react-native-elements';

export default class Test extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loginSuccess: false,
      username: '',
      password: '',
      AUTH_TOKEN: '',
      url: ''
    };
  }

  componentDidMount() {

  }

  render() {
    return (
      this.state.loginSuccess ?
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ height: 60, width: 450, flexDirection: 'row', backgroundColor: '#2C5DB3' }}>
            <Text style={{ color: 'white', fontSize: 18, paddingTop: 16, paddingBottom: 16 }}> ข้อมูลการเข้าชมเว็บไซต์</Text>
            <TouchableOpacity onPress={() => this.logout()} >
              <Image
                style={{ width: 30, height: 30, marginLeft: 110, marginTop: 12 }}
                source={require('./exit.png')}
              />
            </TouchableOpacity>
          </View>
          <WebView
            source={{ uri: `${this.state.url}` }}
            style={{ marginTop: 20, flex: 1 }}>
          </WebView>
        </View>
        :
        <View style={styles.container}>
          <Image
            style={{ width: 350, height: 100 }}
            source={{ uri: 'https://backend.taladwebsite.com/public/uploads/taladwebsite/logo_taladwebsite.png' }}
          />
          <View style={{ backgroundColor: 'white', borderRadius: 10, height: 60, width: 300, marginTop: 20 }}>
            <TextInput
              placeholder="Username or Email"
              style={styles.input}
              onChangeText={(username) => this.setState({ username })}
              value={this.state.username} />
          </View>
          <View style={{ backgroundColor: 'white', borderRadius: 10, height: 60, width: 300, marginTop: 10 }}>
            <TextInput
              placeholder="Password"
              secureTextEntry={true}
              style={styles.input}
              onChangeText={(password) => this.setState({ password })}
              value={this.state.password} />
          </View>
          <TouchableOpacity onPress={() => this.login()} >
            <View style={{ backgroundColor: '#2C5DB3', borderWidth: 1, borderColor: 'white', borderRadius: 10, width: 300, marginTop: 30 }}>
              <Text style={styles.button}>Sign In</Text>
            </View>
          </TouchableOpacity>
          <View style={{ width: 300, marginTop: 10 }}>
            <TouchableOpacity onPress={() => Linking.openURL('https://backend.taladwebsite.com/public/forgot_password')} >
              <Text style={styles.button}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>

        </View >
    );
  }

  loadWebView() {
    axios.defaults.headers.common['Authorization'] = this.state.AUTH_TOKEN;
    axios.post('https://backend.taladwebsite.com/public/api/v1/web_view', {
      page: 'vote'
    })
      .then((response) => {

        if (response.data.status === 200) {
          this.setState({
            url: response.data.url
          });
        }
        else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  login() {
    axios.post('https://backend.taladwebsite.com/public/api/v1/login', {
      email: `${this.state.username}`,
      password: `${this.state.password}`
    })
      .then((response) => {

        if (response.data.status === 200) {
          this.setState({
            loginSuccess: true,
            AUTH_TOKEN: response.data.access_token,
            password: '',
            username: ''
          });
          this.loadWebView();
        }
        else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  logout() {
    axios.defaults.headers.common['Authorization'] = this.state.AUTH_TOKEN;
    axios.post('https://backend.taladwebsite.com/public/api/v1/logout')
      .then((response) => {

        if (response.data.status === 200) {
          this.setState({
            loginSuccess: false
          });
        }
        else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2C5DB3',
  },
  input: {
    fontSize: 18,
    color: 'black',
    textAlign: 'center',
    margin: 10,
  },
  button: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});